# HAODA MIDI 音乐模块


![](./arduinoC/_images/featured.png)

---------------------------------------------------------

## 目录

* [相关链接](#相关链接)
* [描述](#描述)
* [积木列表](#积木列表)
* [示例程序](#示例程序)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新记录](#更新记录)

## 相关链接
* 本项目加载链接: ```https://gitee.com/liliang9693/ext-haodamidi```

* 用户库教程链接: ```https://mindplus.dfrobot.com.cn/extensions-user```


## 描述
支持好搭MIDI模块,可以播放各种MIDI音。

## 积木列表

![](./arduinoC/_images/blocks.png)



## 示例程序

![](./arduinoC/_images/example.png)

## 许可证

MIT

## 支持列表

主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
arduino        |             |        √      |             | 
micro:bit        |             |       √       |             | 
mpython        |             |        √      |             | 


## 更新日志
* V0.0.1  基础功能完成
* V0.0.2  优化掌控板的适配
* V0.0.3  增加其他所有主控板的适配
* V0.0.4  修改音阶与音符无法使用变量问题
* V0.0.5  兼容新版本音阶下拉判断问题
