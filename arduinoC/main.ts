


//% color="#17959d" iconWidth=50 iconHeight=40
namespace haodamidi {
    //% block="haodaMIDI pin#[PIN] set Channel[CH] Tone[YS]" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN" 
    //% CH.shadow="dropdownRound" CH.options="CH" 
    //% YS.shadow="dropdownRound" YS.options="YS" 
    export function set(parameter: any, block: any) {
        let pin=parameter.PIN.code;
        let ch=parameter.CH.code;
        let ys=parameter.YS.code;
       Generator.addInclude("addIncludeHADAMIDI","#include <HAODA_MIDI.h>");
       Generator.addObject("addHDMIDI","HD_MIDI",`midi_${pin}(${pin});`);
       Generator.addCode(`midi_${pin}.Send(${ch},${ys},${ys});`)

    }

    //% block="haodaMIDI pin#[PIN] send Channel[CHB] Scale[YJ] Note[YF] Volume[YL]" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN" 
    //% CHB.shadow="dropdownRound" CHB.options="CHB" 
    //% YJ.shadow="dropdownRound" YJ.options="YJ" 
    //% YF.shadow="dropdownRound" YF.options="YF" 
    //% YL.shadow="range" YL.params.min="0" YL.params.max="127" YL.defl="50"
    export function send(parameter: any, block: any) {

        let pin=parameter.PIN.code;
        let ch=parameter.CHB.code;
        let yj=parameter.YJ.code;
        let yf=parameter.YF.code;
        let yl=parameter.YL.code;

       Generator.addInclude("addIncludeHADAMIDI","#include <HAODA_MIDI.h>");
       Generator.addObject("addHDMIDI","HD_MIDI",`midi_${pin}(${pin});`);
        //console.log((parameter.YJ.parType);
       if((parameter.YJ.parType==="liliang-haodamidi-thirdex.menu.YJMenu") ||  (parameter.YJ.parType==="liliang-haodamidi-thirdex.menu.send.YJMenu")){
        Generator.addCode(`midi_${pin}.Send(${ch},${yj}+${yf},${yl});`)
       }else{
        Generator.addCode(`midi_${pin}.Send(${ch},(((${yj}+1)*12)+(${yf})),${yl});`)
       }

       
    }

    //% block="haodaMIDI pin#[PIN] send Percussion[DJ] Volume[YL]" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN" 
    //% DJ.shadow="dropdownRound" DJ.options="DJ" 
    //% YL.shadow="range" YL.params.min="0" YL.params.max="127" YL.defl="50"
    export function senddj(parameter: any, block: any) {
        let pin=parameter.PIN.code;
        let dj=parameter.DJ.code;
        let yl=parameter.YL.code;
       Generator.addInclude("addIncludeHADAMIDI","#include <HAODA_MIDI.h>");
       Generator.addObject("addHDMIDI","HD_MIDI",`midi_${pin}(${pin});`);
       Generator.addCode(`midi_${pin}.Send(0x99,${dj},${yl});`)
    }

}
