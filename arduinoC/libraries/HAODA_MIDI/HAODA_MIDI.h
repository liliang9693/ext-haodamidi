#ifndef _HAODA_MIDI_H_
#define _HAODA_MIDI_H_
#include "Arduino.h"

class HD_MIDI
{
public:
	HD_MIDI(uint8_t port);
	void write(uint8_t b);
	void Send(uint8_t,uint8_t,uint8_t);
private:
	uint8_t _MIDI_pin;
};
#endif


